---
title: "Présentation de SXIV"
description: "Présentation du visualisateur d'image SXIV"
authors:
  - "jbtremblay@tuta.io (Jean-Benoît Tremblay)"
tags:
  - linux
  - open source
publishedAt: "2020-12-21T10:12:00.000Z"
updatedAt: null
slug: presentation-de-sxiv
---

# Présentation de SXIV

## Qu'est-ce que SXIV ?

SXIV est un visualisateur d'image simple pour [X](https://fr.wikipedia.org/wiki/X_Window_System), comme son sigle anglais le laisse entendre : Simple X Image Viewer. SXIV supporte un grand nombre de formats d'image. Son interface est minimale et on a le choix de la naviguer avec la souris ou le clavier.

## Fonctionnalités

- 2 modes d'opérations
  - Mode image : Permets de visualiser une image. Il est le mode par défaut.
  - Mode vignette(thumbnail) : Permets de visualiser et de sélectionner différents aperçus d'images dans une grille.
- Opérations de visualisation d'image
  - Agrandir l'image
  - Se déplacer dans l'image
  - Effectuer une rotation sur l'image
- Visualisation d'animation GIF
- Mode diaporama. Permets de passer d'une image à l'autre automatiquement après un temps défini.

## Interfaces

Commençons par l'interface visuelle de l'application et cela en mode image. Celle-ci consiste seulement d'une barre d'information situer en bas de l'écran.

![Capture d'écran du mode image de SXIV](/blog/presentation-de-sxiv/img-mode.png)

[//]: # "image hoster sur toile libre: <http://pix.toile-libre.org/?img=1607873663.png>"

Pour ce qui est du mode vignette, une grille des différents aperçus des images est affichée.

![Capture d'écran du mode vignette](/blog/presentation-de-sxiv/tile-mode.png)

[//]: # "image hoster sur toile libre: <http://pix.toile-libre.org/?img=1607873857.png>"

## Utilisation

Pour utiliser SXIV il suffit de spécifier le chemin d'un ou des fichiers.

```shell
sxiv image_1.png ./un_dossier/image_2.jpg
```

Il est aussi possible de le faire avec des dossiers. On peut spécifier l'option -r si l'on veut chercher de manière récursive. Soit dans les dossiers spécifiés ainsi que leurs sous-dossiers.

```shell
sxiv ~/pictures/ -r
```

## Interfaces utilisateur

La souris permet de faire les actions de base alors que le clavier possède une sélection d'action plus complète.

### Souris

- `Clic gauche`
  - Si la souris est dans la partie droite de l'écran on passe à l'image suivante, si elle est située à gauche on retourne à l'image précédente.
- `Clic droit`
  - Active le mode vignette(thumbnail)
- `Molette de la souris`
  - Permet d'agrandir et de réduire l'image
  - Le clic permet de se déplacer dans l'image par apport au positionnement du curseur, et cela pendant que le bouton reste enfoncé.

### Clavier

Il existe plusieurs raccourcis clavier, je vous invite à regarder la page du manuel qui se rapporte à SXIV pour une liste complète.

#### Tous modes

- `q`
  - Quitte le programme.
- `r`
  - Recharge l'image sélectionnée.
- `f`
  - Active le mode plein écran.
- `b`
  - Bascule l'affichage de la barre d'information.
- `+` et `-`
  - Agrandit ou réduit la vue.
- `m`
  - Bascule le marquage de l'image sélectionné.

#### Mode image

- `n` ou la `barre d'espace`
  - Passe à la prochaine image
- `p` ou `backspace`
  - Passe à l'image précédente

Navigation:

- `h`, `j`, `k`, `l` ou les flèches du clavier.

  - Permet de naviguer dans l'image.

- `H`, `J`, `K`, `L`
  - Permet de naviguer aux extrémités de l'image.

Agrandissement:

- `=`
  - Agrandis à 100%.
- `w`
  - Agrandis à 100% et proportionne l'image afin qu'elle couvre la fenêtre en entier.
- `W`
  - Proportionne l'image afin qu'elle couvre la fenêtre en entier.
- `e`
  - Proportionne l'image selon la largeur de la fenêtre.
- `E`
  - Proportionne l'image selon la hauteur de la fenêtre.

Miroir:

- `|`
  - Inverse l'image horizontalement
- `-`
  - Inverse l'image verticalement

Autres:

- `a`
  - Bascule l'antialiasing
- `A`
  - Bascule la visibilité de la transparence de l'image.
- `s`
  - Bascule le mode diaporama(slideshow). Un nombre peut être entré avant d'appuyer sur «s» afin de configurer le nombre de secondes avant de passer à une autre image.

#### Mode vignette

- `h`, `j`, `k`, `l` ou les flèches du clavier.
  - Permet de naviguer dans la liste d'image.
- `R`
  - Recharge toutes les vignettes.

### Configuration des raccourcis clavier

Pour configurer les raccourcis clavier, puisque le programme se veut simple et semble suivre la philosophie du groupe [Suckless.org](https://suckless.org/philosophy/), il faut modifier le fichier source config.c et recompiler le programme. Mais puisque le programme utilise des bibliothèques courantes, donc se trouve probablement en paquet dans les dépôts de votre distribution, la compilation sur un système d'exploitation Linux se fait sans problème.

## Options

- `-f`
  - Lance l'application en mode plein écran
- `-i`
  - Visualise les images dont les chemins ont été spécifiés dans l'entrée standard.
- `-o`
  - Lors de la fermeture, donne en sortie standard les chemins des fichiers marqués.
- `-r`
  - Cherche de manière récursive les dossiers spécifiés.
- `-S`
  - Démarre en mode diaporama, un nombre qui définit le délai entre chaque image doit suivre cette option.

## Extensions et configurations

Il est aussi possible d'utiliser SXIV dans des scripts en interagissant avec le programme par l'entrée et la sortie standard. Si on lance SXIV avec l'option `-o`. Les chemins des images marquées à l'aide de `ctrl+m` lors de l'utilisation de l'application sont envoyés à la sortie standard. Si on le démarre avec l'option `-i`, SXIV ouvrira les images dont les chemins ont été envoyées à son entrée standard.

On peut alors s'imaginer un script, ou une simple commande, qui copie ses fichiers sélectionnés dans un autre dossier.

Exemple:

```shell
sxiv ~/pictures -o | xargs cp -t ~/pictures_selected -v
```

Ici l'argument `-o` spécifie que les chemins des fichiers marqués lors de l'utilisation de SXIV à l'aide de `ctrl+m` sont envoyés à la sortie standard. Cette sortie standard est ensuite redirigée, à l'aide de l'opérateur `|`, vers xargs. Ce programme envoie les fichiers en argument de cp. Ensuite cp copie les fichiers dans le dossier suivant l'argument `-t`. L'option `-v` permet de suivre le progrès des opérations de copies.

On peut aussi ajouter des raccourcis clavier à SXIV. Lorsque l'on appuie sur `CTRL` la prochaine touche du clavier est envoyer à un script externe situer `~/.config/sxiv/exec/key-handler`. Un script qui démontre cette fonctionnalité est inclus lors de l'installation et se situe à `/usr/share/sxiv/exec/key-handler`.

Il est aussi possible de personnaliser la partie gauche de la barre de statut qui affiche les informations du fichier sélectionné à l'aide d'un script. Se script doit être situé à `~/.config/sxiv/exec/image-info`. Un script d'exemple est disponible à `/usr/share/sxiv/exec/image-info`.

## Conclusion

En conclusion, SXIV est un visualisateur d'image qui effectue sa tâche à l'aide d'une interface simple et complète. De plus, il offre des mécanismes qui permettent de le personnaliser et de l'utiliser dans toute sorte de scénarios. Entre autres pour filtrer manuellement des images. Donc, il est parfait pour les systèmes avec peu de ressource ou pour les gens qui préfères les programmes ayant des interfaces graphique simple et une interface clavier complète.

## Liens et sources

[Article publié le 21 décembre sur LinuxFR.](https://linuxfr.org/news/presentation-de-sxiv-un-visualiseur-d-image-minimaliste)

- EN - [Dépôt GIT de l'application](https://github.com/muennich/sxiv)
- EN - [Article du Wiki de Arch Linux portant sur SXIV](https://wiki.archlinux.org/index.php/Sxiv)
- EN - [Page du manuel de SXIV](https://www.mankier.com/1/sxiv)
