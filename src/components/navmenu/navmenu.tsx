import React from "react";
import Link from 'next/link'
import styles from './NavMenu.module.css'

function NavMenu() {
    return (
        <div className={styles["nav-container"]}>
            <nav aria-label="">
                <Link href="/">
                    <a className={styles.logo}> JBTremblay </a>
                </Link>
                <ul>
                    <li>
                        <Link href="/blog">
                            <a className="light-link">Blogue</a>
                        </Link>
                    </li>
                    <li>
                        <Link href="/projets">
                            <a className="light-link">Projets</a>
                        </Link>
                    </li>
                    <li>
                        <Link href="/a-propos">
                            <a className="light-link">À-Propos</a>
                        </Link>
                    </li>
                </ul>
            </nav>
        </div>
    )
}

export default NavMenu;