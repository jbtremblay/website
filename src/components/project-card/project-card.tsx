import React from "react";
import styles from "./project-card.module.css";

interface ProjectCardProps {
  name: string;
  id: string;
  date: string;
  author: JSX.Element;
  imageRef: string;
  summary: JSX.Element;
  description: JSX.Element;
  iframe: JSX.Element;
}

export default function ProjetCard(props: ProjectCardProps): JSX.Element {
  return (
    <article
      data-type={props.id}
      aria-label="Project card"
      id={props.id}
      className={styles["project-card"]}
    >
      <header className={styles["project-card-header"]}>
        <h1>{props.name}</h1>
        <div className={styles["project-card-sub-header"]}>
          <span>{props.author}</span>
          <span>{props.date}</span>
        </div>
      </header>
      <section
        aria-label="Project Description"
        className={styles["project-card-content"]}
      >
        <h2>Description</h2>
        <p>{props.summary}</p>
        <details className={styles["project-card-details"]}>
          <summary className={styles["project-card-summary"]}>
            Informations supplémentaires
          </summary>
          <img src={props.imageRef} className={styles["project-card-img"]} />
          <p>{props.description}</p>
        </details>
        <div>{props.iframe}</div>
      </section>
    </article>
  );
}
