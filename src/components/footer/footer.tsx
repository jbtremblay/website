import Link from "next/link";
import styles from "./footer.module.css";

interface FooterProps {
  buildYear: number;
}

export default function Footer(props: FooterProps): JSX.Element {
  return (
    <footer className={styles.footer}>
      <div>©{" 2021-" + props.buildYear + " "}Jean-Benoît Tremblay</div>
    </footer>
  );
}
