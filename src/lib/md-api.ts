import fs from "fs";
import { join } from "path";
import matter from "gray-matter";
import { BlogPost } from "./models/post";

const postsDirectory = join(process.cwd(), "_blog");

export function getPostSlugs() {
  return fs.readdirSync(postsDirectory);
}

export function getPostBySlug(slug: string, fields: string[] = []): BlogPost {
  const realSlug = slug.replace(/\.md$/, "");
  const fullPath = join(postsDirectory, `${realSlug}.md`);
  const fileContents = fs.readFileSync(fullPath, "utf8");
  const { data, content } = matter(fileContents);

  let post: BlogPost = {
    title: data["title"],
    description: data["description"],
    authors: data["authors"],
    tags: data["tags"],
    publishedAt: data["publishedAt"],
    updatedAt: data["updatedAt"],
    slug: data["slug"],
    content: content,
  };

  return post;
}

export function getAllPosts() {
  const slugs = getPostSlugs();
  const posts = slugs
    .map((slug) => getPostBySlug(slug))
    // sort posts by date in descending order
    .sort((post1, post2) => (post1.publishedAt > post2.publishedAt ? -1 : 1));
  return posts;
}
