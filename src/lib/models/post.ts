export type BlogPost = {
  title: string;
  description: string;
  authors: string[];
  tags: string[];
  publishedAt: string;
  updatedAt: string | null;
  slug: string;
  content: string;
};
