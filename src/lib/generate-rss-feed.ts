import fs from "fs";
import path from "path";
import { getAllPosts } from "./md-api";
import { BlogPost } from "./models/post";

export function getWebsiteURL() {
  const https = "https://";
  const domain = "jbtremblay.com";

  return "".concat(https, domain);
}

export function getPostLink(post: BlogPost) {
  const blogPath = "blog/";
  const postPath = getPostPath(post);

  return "".concat(getWebsiteURL() + "/", blogPath, postPath);
}

export function getPostPath(post: BlogPost) {
  const date = new Date(post.publishedAt);
  const year = date.getFullYear().toString();
  const month = (date.getMonth() + 1).toString();
  const day = date.getDate().toString();

  return `${year}/${month}/${day}/${post.slug}`;
}

export function getFeedLink() {
  const blogPath = "blog/";
  const fileName = "rss.xml";

  return "".concat(getWebsiteURL() + "/", blogPath, fileName);
}

export function getXmlItems(blogPosts: BlogPost[]) {
  return blogPosts
    .map((post) => {
      const link = getPostLink(post);
      const path = getPostPath(post);
      const authors = post.authors.reduce((pV, cV, ci) => {
        return (pV += ", " + cV);
      });
      return `<item>
              <title>${post.title}</title>
              <link>${link}</link>
              <guid>${link}</guid>
              <pubDate>${new Date(post.publishedAt).toUTCString()}</pubDate>
              <description>${post.description}</description>
              <author>${authors}</author>
          </item>
          `;
    })
    .join(""); // Join the array of items into a single long string
}

export function getRssXml(xmlItems: string) {
  const buildDate = new Date(Date.now());
  const url = getWebsiteURL();
  const feedLink = url + "blog/feed";

  return `<?xml version="1.0" ?>
    <rss
      version="2.0" xmlns:atom="http://www.w3.org/2005/Atom"
    >
      <channel>
          <title>Blogue de JBTremblay</title>
          <link>${getWebsiteURL()}</link>
          <description>Blogue à propos de sujets divers liés à la technologie</description>
          <language>fr-CA</language>
          <lastBuildDate>${new Date(buildDate).toUTCString()}</lastBuildDate>
          <atom:link href="${getFeedLink()}" rel="self" type="application/rss+xml" />
          ${xmlItems}
      </channel>
    </rss>`;
}

const postData = getAllPosts();
const xmlItems = getXmlItems(postData);
const rssXml = getRssXml(xmlItems);

fs.writeFile(path.join("public", "blog", "rss.xml"), rssXml, (err) => {
  if (err) {
    console.log(err);
  } else {
    console.log("RSS feed written successfully");
  }
});
