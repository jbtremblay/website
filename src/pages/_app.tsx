import "../styles/globals.css";
import type { AppProps } from "next/app";
import React from "react";
import Head from "next/head";
import NavMenu from "../components/navmenu/navmenu";
import Footer from "../components/footer/footer";
import { Analytics } from "@vercel/analytics/react";

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <>
      <Head>
        <link rel="icon" href="/favicons/favicon.ico" />
        <title>JBTremblay</title>
        <meta name="description" content="Site web personnel de JBTremblay" />
        <link
          href="https://cdnjs.cloudflare.com/ajax/libs/prism-themes/1.9.0/prism-coy-without-shadows.min.css"
          rel="stylesheet"
        />
      </Head>
      <NavMenu></NavMenu>
      <Component {...pageProps} />
      <Footer buildYear={pageProps.buildYear}></Footer>
      <Analytics />
    </>
  );
}

export default MyApp;
