import { NextPage } from "next";
import Link from "next/link";
import React, { FunctionComponent } from "react";
import Home from ".";
import styles from "../styles/404.module.css";

export default function Custom404() {
  return (
    <main className={["content", "container", styles.container].join(" ")}>
      <h1>404 - page introuvable</h1>
      <Link href="/">Retour</Link>
    </main>
  );
}

export async function getStaticProps(context: any) {
  const buildYear = new Date(Date.now()).getFullYear();

  return {
    props: {
      buildYear,
    },
  };
}
