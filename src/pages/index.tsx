import type { NextPage } from "next";
import Head from "next/head";
import Image from "next/image";
import styles from "../styles/Home.module.css";
import Blog from "./blog";

const Home: NextPage = () => {
  return (
    <div className={styles.container}>
      <Head>
        <title>JBTremblay</title>
        <meta name="description" content="Site web personnel de JBTremblay" />
      </Head>
    </div>
  );
};

export default Home;
