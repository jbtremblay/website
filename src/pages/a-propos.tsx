import { NextPage } from "next";
import React from "react";
import profilePic from "../public/a-propos/P1000386-3_500x500.webp";
import Image from "next/image";
import styles from "../styles/About.module.css";
import Link from "next/link";

const About: NextPage = () => {
  return (
    <main
      className={["container", styles["about-container"], "content"].join(" ")}
    >
      <h1>À Propos</h1>

      <Image
        src={profilePic}
        alt="Photo de l'auteur"
        placeholder="blur"
        width={200}
        height={200}
      />
      <p>Jean-Benoit Tremblay</p>
      <p>
        Programmeur analyste chez{" "}
        <a href="https://www.pulsarinformatique.ca/">Pulsar Informatique</a>.
        Diplomé du <a href="https://cchic.ca/">Cégep de Chicoutimi</a> en
        Informatique de Gestion et du BAC en Conception de Jeux-Vidéo de l'
        <a href="https://www.uqac.ca/">UQAC</a>.
      </p>
      <h2 id="contact">
        Contact et <Link href="/projets">projets</Link>
      </h2>
      <p>La manière la plus simple de me contacter est par courriel.</p>
      <ul>
        <li data-type="email">jbtremblay@tuta.io - Adresse courriel</li>
        <li data-type="gitlab">
          <a href="https://gitlab.com/jbtremblay">
            GitLab - Programmes, codes sources et configurations
          </a>
        </li>
        <li data-type="itchio">
          <a href="https://jbtremblay.itch.io/">Itch.io - Jeux Vidéo</a>
        </li>
      </ul>

      <h2>Ce site web</h2>
      <p>
        Réalisé avec
        <br />
        <a href="https://nextjs.org/">
          <img
            src="/icons/nextjs-logo.svg"
            title="ScullyIO Logo"
            style={{ width: "10em" }}
          />
        </a>
      </p>

      <a href="https://gitlab.com/jbtremblay/website">
        <img
          className="icons icons-link"
          src="/icons/git.svg"
          title="Repository Git de ce site web"
        />
        Code source de ce site web
      </a>
    </main>
  );
};

export default About;

export async function getStaticProps(context: any) {
  const buildYear = new Date(Date.now()).getFullYear();

  return {
    props: {
      buildYear,
    },
  };
}
