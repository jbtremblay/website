import { NextPage } from "next";
import React, { Fragment } from "react";
import ProjetCard from "../components/project-card/project-card";
import styles from "../styles/Projects.module.css";

const Projects: NextPage = () => {
  return (
    <main className="container">
      <div className="content">
        <h1>Mes projets</h1>

        <h2>Game jams</h2>

        <p>
          Pour télécharger et jouer à ces jeux, voir mon compte{" "}
          <a href="https://jbtremblay.itch.io/">
            <img
              className="icons icons-link"
              src="/icons/itchio.svg"
              title="mon site web itch.io"
            />
            Itch.io
          </a>
          . Contrairement à cette page, la liste y sera toujours à jour.
        </p>
      </div>
      <div className={styles["project-container"]}>
        <ProjetCard
          name={"Bread"}
          id={"bread"}
          date={"Le 11 octobre 2020"}
          author={
            <Fragment>
              Par <a href="https://regor.itch.io/">Regor</a>
            </Fragment>
          }
          imageRef={"/projects/bread_doggo_man.png"}
          summary={
            <Fragment>
              Bread est un jeu narratif et d'exploration. Le joueur est amené à
              explorer un village et résoudre l'énigme de ses étranges
              habitants.
            </Fragment>
          }
          description={
            <Fragment>
              Le jeu fut créé dans le cadre du{" "}
              <a href="https://itch.io/jam/wonderjam-uqac-automne-2020">
                WonderJam UQAC automne 2020
              </a>
              . Réaliser en 48 heures. Le thème de ce Jam était : « Quand la
              magie tourne mal». Les catégories que nous devions respecter
              étaient : dialogue, exploration et multijoueur. Nous avons retenu
              seulement dialogue et exploration. Bread a été classé à la
              deuxième place par les juges.
            </Fragment>
          }
          iframe={
            <Fragment>
              <iframe
                className={styles["itchio-widget"]}
                src="https://itch.io/embed/784146?linkback=true&amp;bg_color=1f1d1d&amp;fg_color=d9d9d9&amp;link_color=fa5c5c&amp;border_color=444242"
              >
                <a href="https://regor.itch.io/bread">
                  Bread. by Regor, BaronDeRiv, Saint Riversmith, JBTremblay
                </a>
              </iframe>
            </Fragment>
          }
        ></ProjetCard>
        <ProjetCard
          name={"Brutal Bouncer"}
          id={"brutal-bouncer"}
          date={"Le 1 Février 2020"}
          author={
            <Fragment>
              Par <a href="https://regor.itch.io/">Regor</a>
            </Fragment>
          }
          imageRef={"/projects/brutal_bouncer_logo.png"}
          summary={
            <Fragment>
              Brutal Bouncer est un jeu compétitif multijoueur local. Deux
              joueurs, incarnant des gardes du corps, s'affronte afin de
              protéger leurs chanteurs de la foule qui les menacent.
            </Fragment>
          }
          description={
            <Fragment>
              Le jeu fut créé dans le cadre du
              <a href="https://itch.io/jam/wonderjam-uqac-2020-propulse-par-ubisoft-saguenay">
                {" "}
                WonderJam UQAC 2020
              </a>
              . Réaliser en 48 heures. Le thème de ce Jam était simplement :
              frénésie. Nous avons eu comme autre contrainte les trois thèmes
              suivants : compétition, arcade et gestion. Nous avons retenu
              seulement compétition et arcade.
            </Fragment>
          }
          iframe={
            <Fragment>
              <iframe
                className={styles["itchio-widget"]}
                src="https://itch.io/embed/579609?linkback=true&amp;bg_color=083652&amp;fg_color=d9d9d9&amp;link_color=fa5c5c&amp;border_color=6d85f4"
              >
                <a href="https://regor.itch.io/brutal-bouncer">
                  Brutal Bouncer by Regor, BaronDeRiv, Spook_Lord, JBTremblay,
                  Saint Riversmith
                </a>
              </iframe>
            </Fragment>
          }
        ></ProjetCard>
      </div>
    </main>
  );
};

export default Projects;

export async function getStaticProps(context: any) {
  const buildYear = new Date(Date.now()).getFullYear();

  return {
    props: {
      buildYear,
    },
  };
}
