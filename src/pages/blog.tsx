import { GetStaticProps, NextPage } from "next";
import styles from "../styles/Blog.module.css";
import { getAllPosts } from "../lib/md-api";
import { BlogPost } from "../lib/models/post";
import Link from "next/link";
import { Fragment } from "react";
import Image from "next/image";
import rssLogo from "../public/icons/rss.svg";

type Props = {
  allPosts: BlogPost[];
};

const Blog: NextPage<Props> = ({ allPosts }) => {
  const years = allPosts.reduce<Number[]>(function (pV, cV, i) {
    const date = new Date(cV.publishedAt);
    const year = date.getFullYear();

    if (!pV.includes(year)) pV.push(year);
    return pV;
  }, []);

  function getPostByYear(year: Number) {
    return allPosts
      .filter((post) => new Date(post.publishedAt).getFullYear() === year)
      .sort(
        (a, b) =>
          new Date(b.publishedAt).getTime() - new Date(a.publishedAt).getTime()
      );
  }

  return (
    <main className="container content">
      <h1>Blogue</h1>
      <p>Bienvenue sur mon blogue.</p>
      {years.map((year) => {
        return (
          <Fragment key={year.toString()}>
            <h2>{year}</h2>
            {getPostByYear(year).map((blogPost) => {
              const date = new Date(blogPost.publishedAt);
              const month = (date.getMonth() + 1).toString();
              const day = date.getDate().toString();

              return (
                <li key={blogPost.publishedAt}>
                  <Link
                    href={`/blog/${year}/${month}/${day}/${encodeURIComponent(
                      blogPost.slug
                    )}`}
                  >
                    <a>
                      {`${new Date(blogPost.publishedAt)
                        .toLocaleDateString("fr-CA")
                        .slice(5)} ${blogPost.title}`}
                    </a>
                  </Link>
                </li>
              );
            })}
          </Fragment>
        );
      })}
      <hr />
      <p>
        Abonnez-vous au{" "}
        <Link href="/blog/rss.xml">
          <a>
            <span className="icons icons-link">
              <Image src={rssLogo} width={25} height={25} />
            </span>
            Flux RSS
          </a>
        </Link>
      </p>
    </main>
  );
};

export default Blog;

export const getStaticProps: GetStaticProps = async (context) => {
  const allPosts = getAllPosts();
  const buildYear = new Date(Date.now()).getFullYear();

  return {
    props: { allPosts, buildYear },
  };
};
