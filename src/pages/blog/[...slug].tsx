import { GetStaticPaths, GetStaticProps, NextPage } from "next";
import markdownToHtml from "../../lib/markdownToHtml";
import { getAllPosts, getPostBySlug } from "../../lib/md-api";
import { BlogPost } from "../../lib/models/post";
import styles from "../../styles/Post.module.css";

type Props = {
  post: BlogPost;
  content: string;
};

const Post: NextPage<Props> = ({ post, content }) => {
  return (
    <main className="container content">
      {post.publishedAt != null && (
        <div>
          Publié le{" "}
          {new Date(post.publishedAt).toLocaleDateString("fr-CA", {
            day: "numeric",
            month: "long",
            year: "numeric",
          })}
        </div>
      )}
      {post.updatedAt != null && (
        <div>
          Dernière modification le{" "}
          {new Date(post.updatedAt).toLocaleDateString("fr-CA", {
            day: "numeric",
            month: "long",
            year: "numeric",
          })}
        </div>
      )}
      <hr />
      <div
        className={styles["content-html"]}
        dangerouslySetInnerHTML={{ __html: content }}
      />
    </main>
  );
};

export default Post;

export const getStaticProps: GetStaticProps = async (context) => {
  const path = context.params?.slug as string[];
  const slug = path[3];
  const post = getPostBySlug(slug ? slug : "");
  const buildYear = new Date(Date.now()).getFullYear();

  if (!post) {
    return { redirect: { permanent: false, destination: "/blog/" } };
  }

  const content = await markdownToHtml(post.content || "");

  return {
    props: { post, content, buildYear },
  };
};

export const getStaticPaths: GetStaticPaths = async (context) => {
  let posts = getAllPosts();

  let paths = posts.map((post) => {
    const date = new Date(post.publishedAt);
    const year = date.getFullYear().toString();
    const month = (date.getMonth() + 1).toString();
    const day = date.getDate().toString();

    return {
      params: { slug: [year, month, day, post.slug] },
    };
  });
  return {
    paths: paths,
    fallback: false,
  };
};
